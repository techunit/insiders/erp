
#Clear all
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q)


#Build & run database container for odoo
docker build -t techunit/odoo-db:v0 ./odoo-db
docker run -d --name odoo-db techunit/odoo-db:v0

#Build & run backend container for odoo
docker build -t techunit/odoo-backend:v0 ./odoo-backend
docker run -p 8069:8069 --name odoo-backend --link odoo-db:db -t techunit/odoo-backend:v0
